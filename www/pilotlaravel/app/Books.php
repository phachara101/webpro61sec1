<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = "books";
    //ORM --> search in google it is __

    protected $fillable = ['name','category','description']; // ฟิลที่สามาถเพิ่มเข้าไปได้ หากใช้ฟังชั่น all
}
