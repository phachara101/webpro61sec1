<?php

namespace App\Http\Controllers;

use App\Books;
use function foo\func;
use Illuminate\Http\Request;

class Bookcontroller extends Controller
{
   /* function store(Request $request)
    {
        $books = new Books();
        $books->name = $request->name;
        $books->setAttribute("name",$request->name);
        $books->setAttribute("category",$request->category);
        $books->setAttribute("description",$request->description);
        // หาก $request->all มันจะใส่ให้ทั้งหมด ไม่ต้องมาพิมพ์เลือกเอง
        if ($books->save())
        {
            return true;
            // หากทำการ save จะ return ค่า true ออกมา
        }

    }*/
    function store(Request $request)
    {
        $book = new Books();
        if(Books::created($request->all()))
        {
            return true;
        }
    }
    function update(Request $request,Books $book)
    {
        if($book->fill($request->all())->save())
        {

            return true;
            // หากจะ update บางฟิล ต้องทำยังไง
        }
    }
    function index()
    {
        $books = Books::all(); // นี่คือ select * from table
        return books;
    }
    function show(Books $books)
    {
        return $books;
    }
    function destroy(Books $book)
    {
        if($book->delete())
        {
            return true;
        }
    }
}

// Books คือ Model
// ต้องเขียน Control ไว้ควบคุม Model ด้วย
// Model มีไว้เพื่อเชื่อมต่อกับ DB