<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',256)->charset('utf8');
            $table->string("category",256)->charset("utf8");
            $table->text("description")->charset("utf8");
            $table->timestamps(); // บอกว่าเราเก็บข้อมูลนี้ตอนไหน
            // ข้อดีของตัวนี้คือ save ดีกว่า mysql pro ไม่ต้องมาสร้าง project เองใหม่เรื่อยๆ มันจะสร้างเองจาก code เลย

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('books', function (Blueprint $table) {
            //
        });
    }
}
